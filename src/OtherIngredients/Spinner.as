package OtherIngredients
{
	import flash.geom.Point;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import Controller.MediaPlayerControler;
	
	public class Spinner extends Sprite
	{
		private var _roller:DisplayObject;
		private var _bg:DisplayObject;
		private var _progressLayer:DisplayObject;
		private var _loadLine:DisplayObject;
		
		private var _rollerIsPushed:Boolean = false;
		
		public function Spinner(roller:DisplayObject, bg:DisplayObject, progressLayer:DisplayObject)
		{
			this._roller = roller;
			this._bg = bg; 
			this._progressLayer = progressLayer;
			
			this.addEventListener(Event.ADDED_TO_STAGE, initialize);
			
			super();
		}
		
		private function initialize(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			this._progressLayer.width = 0;
			this._roller.x = 0; 
			
			this.addChild(this._bg);
			this.addChild(this._progressLayer);
			this.addChild(this._roller);
		}
		
		public function getProgresingPercent():Number
		{
			return (this._roller.x / this._bg.width);
		}
		
		public function updateProgessing(coeficent:Number):void
		{
			this._progressLayer.width = this._bg.width * coeficent;
			this._roller.x = this._progressLayer.width; 
		}
		
		public function addLayer(anotherLayer:DisplayObject):void
		{
			this._loadLine = anotherLayer;
			this._loadLine.alpha = 0.3;
			addChildAt(this._loadLine,1);
		}
		
		public function updateAdditionalLayer(coeficent:Number):void
		{
			this._loadLine.width = this._bg.width * coeficent;
		}
		
		public function addTouchListener():void
		{
			if(!this.hasEventListener(TouchEvent.TOUCH))
			{
				this.addEventListener(TouchEvent.TOUCH, touchHandler);
			}
		}
		
		private function touchHandler(event:TouchEvent):void
		{
			var touch:Touch = event.getTouch(event.currentTarget as DisplayObject);
			
			if(touch)
			{
				var globalPoint:Point = new Point(touch.globalX, touch.globalY);
				var localPoint:Point = globalToLocal(globalPoint);
				
				if(touch.phase == TouchPhase.BEGAN)
				{
					this._roller.x = localPoint.x;
					this._progressLayer.width = localPoint.x;
					this._roller.scaleX = this._roller.scaleY = stage.stageWidth * 0.0007; 
					this._rollerIsPushed = true;
					
					dispatchEvent(new Event("rollerIsPushed"));
				}
				else if(touch.phase == TouchPhase.ENDED)
				{
					this._rollerIsPushed = false; 
					this._roller.scaleX = this._roller.scaleY = stage.stageWidth * 0.0006;
					MediaPlayerControler.instance.setLastPosition(this._roller.x / this._bg.width);
					
					dispatchEvent(new Event("rollerIsUnpushed"));
				}
				else
				{
					if(this._rollerIsPushed)
					{
						globalPoint = new Point(touch.globalX, touch.globalY);
						localPoint = globalToLocal(globalPoint);
						if(localPoint.x < 0)
						{
							this._roller.x = 0;
							this._progressLayer.width = 0;
						}
						else if(localPoint.x > this._bg.width)
						{
							this._roller.x = this._bg.width - 1;
							this._progressLayer.width = this._bg.width - 1;
						}
						else
						{
							this._progressLayer.width = localPoint.x;
							this._roller.x = localPoint.x;
						}
					}
				}
			}
		}
	}
}