package
{
	import flash.display.Sprite;
	
	import View.MediaPlayerView;
	
	import starling.core.Starling;
	
	[SWF(width='800', height='800', backgroundColor='#ffffff', frameRate='30')]
	
	
	public class Player extends Sprite
	{
		public function Player()
		{
			/*var windowInitOptions:NativeWindowInitOptions = new NativeWindowInitOptions();
			windowInitOptions.minimizable = true;
			windowInitOptions.resizable = false;
			windowInitOptions.maximizable = false;
			windowInitOptions.systemChrome = NativeWindowSystemChrome.STANDARD;
			windowInitOptions.transparent = false;
			
			var win:NativeWindow = new NativeWindow(windowInitOptions);
			win.activate()*/
			
			Starling.multitouchEnabled = true;
			
			var myStarling:Starling = new Starling(MediaPlayerView, this.stage);
			myStarling.antiAliasing = 4;
			myStarling.simulateMultitouch = true;
			myStarling.start();
		}
		
	}
}