package
{
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class Assets
	{
		[Embed(source="../assets/Shine.png")]
		public static const Shine:Class;
		
		[Embed(source="../assets/Shine1.png")]
		public static const Shine1:Class;
		
		[Embed(source="../assets/Vinil.png")]
		public static const Vinil:Class;
		
		[Embed(source="../assets/spritesheets/mySpriteSheets.png")]
		public static const AtlasGameTexture:Class;
		
		[Embed(source="../assets/spritesheets/mySpriteSheets.xml", mimeType="application/octet-stream")]
		public static const AtlasGameXML:Class;
		
		private static var gameTextureAtlas:TextureAtlas;
		private static var gameTextures:Dictionary = new Dictionary; 
		
		public static function getAtlas():TextureAtlas
		{
			if(gameTextureAtlas == null)
			{
				var texture:Texture = Texture.fromEmbeddedAsset(AtlasGameTexture);
				var xml:XML = XML(new AtlasGameXML());
				gameTextureAtlas = new TextureAtlas(texture, xml);
			}
			return gameTextureAtlas;
		}
		
		public static function getTexture(name:String):Texture
		{
			if(gameTextures[name] == undefined)
			{
				var bitmap:Bitmap = new Assets[name]();
				gameTextures[name] = Texture.fromBitmap(bitmap);
			}
			return gameTextures[name];
		}
		
	}
}