package Model
{
	import OtherIngredients.SongVO;
	
	import View.MediaPlayerView;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;

	public class MediaPlayerModel extends EventDispatcher
	{
		private var _owner:MediaPlayerView;
		private var _play:Boolean = false; 
		private var _currentSong:String = new String;
		private var _currentSongIndex:int = -1;
		private var _soundList:Vector.<SongVO>;
		private var _listIsFull:Boolean = false; 
		private var _listIsUpdated:Boolean = false;
		private var _rollIsPushed:Boolean = false; 
		private var _songEnded:Boolean = false; 
		private var _volumeButtonIsPushed:Boolean = false;
		
		public function MediaPlayerModel(owner:MediaPlayerView)
		{
			this._owner = owner;
		}
	
		public function set listIsUpdated(value:Boolean):void
		{
			_listIsUpdated = value;
			dispatchEvent(new Event("AddSongsInList"));
		}

		public function get volumeButtonIsPushed():Boolean
		{
			return _volumeButtonIsPushed;
		}

		public function set volumeButtonIsPushed(value:Boolean):void
		{
			_volumeButtonIsPushed = value;
			dispatchEvent(new Event("volumeIsPushed"));
		}

		public function set songEnded(value:Boolean):void
		{
			_songEnded = value;
			dispatchEvent(new Event("SongEnded"));
		}

		public function get currentSongIndex():int
		{
			return _currentSongIndex;
		}

		public function set currentSongIndex(value:int):void
		{
			_currentSongIndex = value;
		}

		public function get rollIsPushed():Boolean
		{
			return _rollIsPushed;
		}

		public function set rollIsPushed(value:Boolean):void
		{
			_rollIsPushed = value;
		}

		public function get listIsFull():Boolean
		{
			return _listIsFull;
		}

		public function set listIsFull(value:Boolean):void
		{
			if(this._listIsFull)
			{
				dispatchEvent(new Event("updateList"));
			}
			if(value == true)
			{
				dispatchEvent(new Event("loadComplete"));
			}
			_listIsFull = value;
		}

		public function get soundList():Vector.<SongVO>
		{
			return _soundList;
		}

		public function set soundList(value:Vector.<SongVO>):void
		{
			_soundList = value;
		}

		public function get currentSong():String
		{
			return _currentSong;
		}

		public function set currentSong(value:String):void
		{
			_currentSong = value;
		}

		public function get play():Boolean
		{
			return _play;
		}

		public function set play(value:Boolean):void
		{
			_play = value;
			dispatchEvent(new Event("playOrStop"));
		}

		public function get owner():MediaPlayerView
		{
			return _owner;
		}
	}
}