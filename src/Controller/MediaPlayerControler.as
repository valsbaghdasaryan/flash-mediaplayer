package Controller
{
	import flash.events.Event;
	import flash.events.FileListEvent;
	import flash.filesystem.File;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.FileFilter;
	import flash.net.URLRequest;
	
	import Model.MediaPlayerModel;
	
	import OtherIngredients.SongVO;
	
	import starling.events.Event;

	public class MediaPlayerControler
	{
		
		private static var _instance:MediaPlayerControler; 
		
		private var _sound:Sound;
		private var _model:MediaPlayerModel; 
		private var _soundChanel:SoundChannel;
		private var _lastPosition:Number;
		private var _lastSongName:String;
		private var _fileFilter:FileFilter;
		private var _myFiles:File;

		private var trans:SoundTransform = new SoundTransform(1);;
		
		
		public static function get instance():MediaPlayerControler
		{
			if(!_instance)
			{
				_instance = new MediaPlayerControler();
			}
			return _instance;
		}
		
		public function MediaPlayerControler()
		{
			if(_instance)
			{
				throw new Error ("MediaPlayerControler: an instance is already exist)");
			}
		}
		
		public function getPositionProgres():Number
		{
			if(this._sound.length < 1)
			{
				return (this._soundChanel.position / 1);
			}
			return (this._soundChanel.position / this._sound.length)
		}
		
		public function getBytesLoadedPescent():uint
		{
			return (this._sound.bytesLoaded / this._sound.bytesTotal) 
		}
		
		public function setLastPosition(value:Number):void
		{
			if(this._model.currentSong)
			{
				this._lastPosition = value * this._sound.length;
			}
			else
			{
				return;
			}
		}
		
		public function loadSong(url:String):void
		{
			if(this._lastSongName != url)
			{
				this._sound = new Sound(new URLRequest(url));
				this._soundChanel = this._sound.play(0);
				this._soundChanel.soundTransform = trans;
				this._soundChanel.stop();
			}
			this._lastSongName = url; 
		}
		
		private function soundEndedListener(event:flash.events.Event):void
		{
			this._lastPosition = 0; 
			this._model.play = false;
			this._model.songEnded = true;
			this._soundChanel.removeEventListener(flash.events.Event.SOUND_COMPLETE, soundEndedListener);
		}
		
		public function initialize(model:MediaPlayerModel):void
		{
			this._model = model;
			this._model.addEventListener("playOrStop", playOrStopHandler);
			
			this.createFileReferance();
		}
		
		private function createFileReferance():void
		{
			this._fileFilter = new FileFilter("Sounds", "*.mp3");
			this._myFiles = new File;
			//this._myFiles.addEventListener(flash.events.Event.CANCEL, filesCanceledHandler);
		}
		
		private function filesSelectedHandler(event:flash.events.FileListEvent):void
		{
			this._myFiles.removeEventListener(FileListEvent.SELECT_MULTIPLE, filesSelectedHandler);
			this._model.soundList = new Vector.<SongVO>;
			var i:int;
			var songVO:SongVO;
			for (i = 0; i < event.files.length; i ++)
			{
				songVO = new SongVO;
				songVO.num = i;
				songVO.path = String(event.files[i].nativePath);
				songVO.title = String(event.files[i].name);
				this._model.soundList.push(songVO);
			}
			this._model.listIsFull = true;
		}
		
		private function addFilesSelectedHandler(event:flash.events.FileListEvent):void
		{
			this._myFiles.removeEventListener(FileListEvent.SELECT_MULTIPLE, addFilesSelectedHandler);
			if(!this._model.soundList)
			{
				filesSelectedHandler(event);
			}
			else
			{
				var songVO:SongVO;
				var beganIndex:int = this._model.soundList.length;
				for (var i:int = 0; i < event.files.length; i ++)
				{
					songVO = new SongVO;
					songVO.num = beganIndex + i;
					songVO.path = String(event.files[i].nativePath);
					songVO.title = String(event.files[i].name);
					this._model.soundList.push(songVO);
				}
				this._model.listIsUpdated = true;
			}
		}
		
		public function browseHandler():void
		{
			this._myFiles.browseForOpenMultiple("Open Tracks",[this._fileFilter]);
			this._myFiles.addEventListener(FileListEvent.SELECT_MULTIPLE, filesSelectedHandler);
		}
		
		public function addBrowseHandler():void
		{
			this._myFiles.browseForOpenMultiple("Add Tracks",[this._fileFilter]);
			this._myFiles.addEventListener(FileListEvent.SELECT_MULTIPLE, addFilesSelectedHandler);
		}
		
		public function volumeHandler(coeficent:Number):void
		{
			if(this._model.currentSong)
			{
				trans = new SoundTransform(coeficent);
				this._soundChanel.soundTransform = trans;
			}
			else
			{
				///////////
				trans = new SoundTransform(coeficent);
			}
		}
			
		
		private function playOrStopHandler(event:starling.events.Event):void
		{
			if(this._model.play)
			{
				this._soundChanel = this._sound.play(this._lastPosition);
				this._soundChanel.soundTransform = trans;
				this._soundChanel.addEventListener(flash.events.Event.SOUND_COMPLETE, soundEndedListener);
			}
			else
			{
				this._lastPosition = this._soundChanel.position;
				this._soundChanel.stop();
				this._soundChanel.removeEventListener(flash.events.Event.SOUND_COMPLETE, soundEndedListener);
			}
		}
	}
}