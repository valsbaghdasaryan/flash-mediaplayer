package View
{
import Controller.MediaPlayerControler;

import Model.MediaPlayerModel;

import OtherIngredients.SongVO;
import OtherIngredients.Spinner;

import feathers.controls.ScrollContainer;
import feathers.layout.VerticalLayout;

import starling.display.Button;
import starling.display.DisplayObject;
import starling.display.Image;
import starling.display.Quad;
import starling.display.Sprite;
import starling.events.Event;
import starling.events.Touch;
import starling.events.TouchEvent;
import starling.events.TouchPhase;
import starling.text.TextField;
import starling.textures.Texture;
import starling.utils.HAlign;

public class MediaPlayerView extends Sprite
	{
		private var _model:MediaPlayerModel;
		
		private var _texture:Texture;
		
		private var _playButton:Button;
		private var _pauseButton:Button;
		private var _previousButton:Button;
		private var _nextButton:Button;
		private var _listButton:Button;
		private var _soundButton:Button;
		private var _browseButton:Button;
		private var _addBrowseButton:Button;

		private var _listMenu:Image;
		private var _tube:Image;
		private var _panel:Image;
		private var _vinil:Image;

		private var _listContainer:ScrollContainer; 
		private var _panelContainer:Sprite = new Sprite;
		
		private var _spinner:Spinner;
		private var _volumeSpinner:Spinner;
		
		private var _unSelectedColor:int = 0x0085e4;
		private var _selectedColor:int = 0xff6600;
		private var _lastSelected:TextField;
		private var _trackRollXCoord:Number;
		
		private var _paused:Boolean; 
		
		public function MediaPlayerView()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE,initialize);
		}
		
		private function initialize(event:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE,initialize);
			this._model = new MediaPlayerModel(this);
			
			MediaPlayerControler.instance.initialize(this._model);
			this._model.addEventListener("loadComplete", loadCompleteHandler);
			this._model.addEventListener("playOrStop", playOrStopHandler);
			this._model.addEventListener("SongEnded", songEndedHandler);
			this._model.addEventListener("volumeIsPushed", volumeHandler);
			this._model.addEventListener("updateList", updateListContainer);
			this._model.addEventListener("AddSongsInList", AddingListSongsHandler);
			
			
			createVinil();
			createPanel();
			createSpinner();
			createVolumeSpiner();
			createListContainer();
			createTube();
			
			this._panelContainer.addEventListener(Event.TRIGGERED,touchHandler);
		}
		
		private function loadCompleteHandler(e:Event):void
		{
			createList(false);
			this._spinner.addTouchListener();
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		private function AddingListSongsHandler(event:Event):void
		{
			var isPushed:Boolean = this._listContainer.visible;
			var currentSongIndex:int = this._model.currentSongIndex;

			
			this._listContainer.removeChildren(0, this._listContainer.numChildren, true);
			this._listContainer.parent.removeChild(this._listContainer);
			this._listContainer = null;
			createListContainer();
			this._listContainer.visible = isPushed;
			
			createList(true);

			this._model.currentSongIndex = currentSongIndex;
			this._model.currentSong = this._model.soundList[currentSongIndex].path;
			
			updateSelectedSongTextfield(this._model.soundList[this._model.currentSongIndex].textfield);
			this._spinner.addTouchListener();
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		
		private function volumeHandler(event:Event):void
		{
			if(this._model.volumeButtonIsPushed)
			{
				this._volumeSpinner.visible = true; 
			}
			else
			{
				this._volumeSpinner.visible = false;
			}
		}
		
		private function createVolumeSpiner():void
		{
			var backLine:Image = new Image(Assets.getAtlas().getTexture("Track"));
			
			backLine.width = stage.stageWidth * 0.08;
			backLine.height = stage.stageWidth * 0.01;
			backLine.pivotY = backLine.height / 2;
			
			var roller:Image = new Image(Assets.getAtlas().getTexture("TrackRoll"));
			scalingPanel(roller);
			roller.scaleX = roller.scaleY = stage.stageWidth * 0.0006;
			
			var followLine:Quad = new Quad(1,backLine.height, 0x880000);
			followLine.pivotY = followLine.height / 2;
			
			this._volumeSpinner = new Spinner(roller, backLine, followLine);
			this._panelContainer.addChild(this._volumeSpinner);
			this._volumeSpinner.x = this._soundButton.x;
			this._volumeSpinner.y = this._soundButton.y - this._soundButton.height / 2 - roller.width / 2;
			this._volumeSpinner.rotation = - Math.PI / 2;
			this._volumeSpinner.addEventListener("rollerIsUnpushed", volumeRollerUnpushedHandler);
			this._volumeSpinner.visible = false;
			this._volumeSpinner.updateProgessing(1);
			this._volumeSpinner.addTouchListener();
				
		}
		
		private function volumeRollerUnpushedHandler(event:Event):void
		{
			MediaPlayerControler.instance.volumeHandler(this._volumeSpinner.getProgresingPercent());
		}
		
		private function songEndedHandler(event:Event):void
		{
			var play:Boolean = true;
			if(this._model.currentSongIndex == this._model.soundList.length - 1)
			{
				play = false
			}
			nextSong();
			this._model.play = play
			
			
		}
		
		private function updateListContainer(event:Event):void
		{
			var isPushed:Boolean = this._listContainer.visible;

			this._model.play = false; 
			MediaPlayerControler.instance.setLastPosition(0);
			this._spinner.updateProgessing(0);
			//MediaPlayerControler.instance.loadSong(this._model.currentSong);
			
			this._listContainer.removeChildren(0, this._listContainer.numChildren, true);
			this._listContainer.parent.removeChild(this._listContainer);
			this._listContainer = null;
			createListContainer();
			this._listContainer.visible = isPushed;
		}
		
		private function createSpinner():void
		{
			var trackLine:Image = new Image(Assets.getAtlas().getTexture("Track"));
			trackLine.pivotY = trackLine.height / 2;
			trackLine.scaleX = stage.stageWidth * 0.0005;
			trackLine.scaleY = stage.stageWidth * 0.0007;
			
			var roller:Image = new Image(Assets.getAtlas().getTexture("TrackRoll"));
			scalingPanel(roller);
			roller.scaleX = roller.scaleY = stage.stageWidth * 0.0006;
			this._trackRollXCoord = trackLine.x - trackLine.width / 2 + roller.width * 0.2;
			
			var playLine:Quad = new Quad(1,trackLine.height *0.9, 0x880000);
			playLine.pivotY = playLine.height / 2;
			
			var loadLine:Quad = new Quad(1, trackLine.height *0.9, 0x999999);
			loadLine.pivotY = loadLine.height / 2;
			
			this._spinner = new Spinner(roller, trackLine, playLine);
			this._spinner.pivotY = this._spinner.height / 2; 
			this._spinner.x = - this._panel.width * 0.27;
			this._panelContainer.addChild(this._spinner);
			this._spinner.addEventListener("rollerIsPushed", rollerPushedHandler);
			this._spinner.addEventListener("rollerIsUnpushed", rollerUnpushedHandler);
			this._spinner.addLayer(loadLine);
		}
		
		private function rollerPushedHandler(event:Event):void
		{
			this._paused = !this._model.play;
			this._model.play = false;
		}
		
		private function rollerUnpushedHandler(event:Event):void
		{
			if(!this._paused)
			{
				this._model.play = true;
			}
		}
		
		private function playOrStopHandler(event:Event):void
		{
			if(this._model.play)
			{
				this._playButton.visible = false;
				this._pauseButton.visible = true;
			}
			else
			{
				this._playButton.visible = true;
				this._pauseButton.visible = false;
			}
		}
		
		
		private function createTube():void
		{
			this._tube = new Image(Assets.getAtlas().getTexture("1111"));
			addChild(this._tube)
			
			this._tube.pivotX = this._tube.width / 2;
			this._tube.pivotY = this._tube.height / 2;
			this._tube.scaleX = this._tube.scaleY = stage.stageWidth * 0.00065;

			this._tube.x = this._vinil.x * 1.58
			this._tube.y = this._vinil.y * 0.75
		}
		
		private function createList(adding:Boolean):void
		{
			var i:int; 
			var textField:TextField;
			for(i = 0; i < this._model.soundList.length; i ++)
			{
				var fontSize:Number = stage.stageWidth * 0.03
				textField = new TextField(this._listMenu.width, fontSize, this._model.soundList[i].title);
				textField.hAlign = HAlign.LEFT;
				textField.color = this._unSelectedColor;
				//textField.name = i.toString();
				textField.addEventListener(TouchEvent.TOUCH, listTouchHandler);
				this._model.soundList[i].textfield = textField;
				this._listContainer.addChild(textField);
				if(i == 0 && !adding)				
				{
					selectFirstSong(textField);
				}
			}
		}
		
		private function selectFirstSong(textField:TextField):void
		{
			textField.color = this._selectedColor;
			textField.bold = true;
			this._lastSelected = textField;
			
			this._model.currentSong = this._model.soundList[0].path; 
			this._model.currentSongIndex = 0;
			MediaPlayerControler.instance.loadSong(this._model.currentSong);
		}
		
		private function createListContainer():void
		{
			var layout:VerticalLayout = new VerticalLayout;
			var paddingCoeficent:Number = stage.stageHeight * 0.03;
			
			this._listMenu = new Image(Assets.getAtlas().getTexture("NamesPanel"));
			this._listMenu.width = this._panel.width * 0.9;
			
			this._listContainer = new ScrollContainer;
			this._listContainer.backgroundSkin = this._listMenu;
			this._listContainer.setSize(this._listMenu.width, this._listMenu.height);
			this._listContainer.x = this._panelContainer.x - this._panelContainer.width * 0.966;
			this._listContainer.y = this._panelContainer.y;
			this._listContainer.layout = layout;
            this._listContainer.hasElasticEdges = false;
			this._listContainer.paddingLeft = paddingCoeficent;
			this._listContainer.paddingBottom = paddingCoeficent;
			this._listContainer.paddingTop = paddingCoeficent / 2;
			this._listContainer.paddingRight = paddingCoeficent;
			
			this.addChild(this._listContainer);
			
			this._listContainer.visible = false;
		}
		
		private function listTouchHandler(event:TouchEvent):void
		{
			
			var touch:Touch = event.getTouch(event.currentTarget as DisplayObject, TouchPhase.ENDED);
			var text:TextField = event.currentTarget as TextField;
			
			if(touch)
			{
				var selectedSongVO:SongVO = searchSongPath(text.text);
				this._model.play = false; 
				text.bold = true;
				text.color = this._selectedColor;
				this._model.currentSong = selectedSongVO.path;
				this._model.currentSongIndex = selectedSongVO.num;
				updateSelectedSongTextfield(text);
				MediaPlayerControler.instance.loadSong(this._model.currentSong);
				MediaPlayerControler.instance.setLastPosition(0);
				this._model.play = true;
			}
		}
		
		private function searchSongPath(name:String):SongVO
		{
			var i:int;
			for(i = 0; i < this._model.soundList.length; i ++)
			{
				if(this._model.soundList[i].title == name)
				{
					return this._model.soundList[i]
				}
			}
			trace("error");
			return this._model.soundList[-1];
		}
		
		private function updateSelectedSongTextfield(text:TextField):void
		{
			text.bold = true;
			text.color = this._selectedColor;
			if(this._lastSelected != null)
			{
				if(this._lastSelected != text)
				{
					this._lastSelected.color = this._unSelectedColor;
					this._lastSelected.bold = false;
				}
			}
			this._lastSelected = text;
		}
		
		private function touchHandler(e:Event):void
		{
			if(e.target == this._playButton)
			{
				if(this._model.listIsFull)
				{
					MediaPlayerControler.instance.loadSong(this._model.currentSong);
					this._model.play = true;
				}
			}
			if(e.target == this._pauseButton)
			{
				this._model.play = false;
			}
			if(e.target == this._listButton)
			{
				this._listContainer.visible = !this._listContainer.visible;
			}
			if(e.target == this._nextButton && this._model.listIsFull)
			{
				nextSong();
			}
			if(e.target == this._previousButton  && this._model.listIsFull)
			{
				previousSong();
			}
			if(e.target == this._soundButton)
			{
				this._model.volumeButtonIsPushed = !this._model.volumeButtonIsPushed;
			}
			if(e.target == this._browseButton)
			{
				MediaPlayerControler.instance.browseHandler();
			}
			if(e.target == this._addBrowseButton)
			{
				MediaPlayerControler.instance.addBrowseHandler();
			}
		}
		
		private function nextSong():void
		{
			this._paused = !this._model.play;
			if(this._model.currentSongIndex + 1 > this._model.soundList.length - 1)
			{
				this._model.play = false;
				this._spinner.updateProgessing(0);
				MediaPlayerControler.instance.setLastPosition(0);
				
			}
			else
			{
				this._model.play = false;
				this._model.currentSongIndex ++; 
				this._model.currentSong = this._model.soundList[this._model.currentSongIndex].path;
				MediaPlayerControler.instance.loadSong(this._model.currentSong);
				MediaPlayerControler.instance.setLastPosition(0);
				if(!this._paused)
				{
					this._model.play = true;
				}
				else
				{
					this._spinner.updateProgessing(0);
				}
				updateSelectedSongTextfield(this._model.soundList[this._model.currentSongIndex].textfield);
			}
		}
		
		private function previousSong():void
		{
			this._paused = !this._model.play;
			if(this._model.currentSongIndex - 1 < 0)
			{
				this._model.play = false;
				this._spinner.updateProgessing(0);
				MediaPlayerControler.instance.setLastPosition(0);
			}
			else
			{
				this._model.play = false;
				this._model.currentSongIndex --; 
				this._model.currentSong = this._model.soundList[this._model.currentSongIndex].path;
				MediaPlayerControler.instance.loadSong(this._model.currentSong);
				MediaPlayerControler.instance.setLastPosition(0);
				if(!this._paused)
				{
					this._model.play = true;
				}
				else
				{
					this._spinner.updateProgessing(0);
				}
				updateSelectedSongTextfield(this._model.soundList[this._model.currentSongIndex].textfield);
			}
		}
		
		private function createVinil():void
		{
			this._texture = Assets.getTexture("Vinil");
			this._vinil = new Image(this._texture);
			addChild(this._vinil);
			this._vinil.x = stage.stageWidth / 2;
			this._vinil.y = stage.stageHeight / 2 - stage.stageWidth * 0.027;
			scaling(this._vinil);
		}
		
		public function enterFrameHandler(e:Event):void
		{
			if(this._model.play)
			{
				this._vinil.rotation +=0.01;
				this._spinner.updateProgessing(MediaPlayerControler.instance.getPositionProgres());
			}
			this._spinner.updateAdditionalLayer(MediaPlayerControler.instance.getBytesLoadedPescent());
		}
		
		private function createPanel():void
		{
			addChild(this._panelContainer);
			
			this._panel = new Image(Assets.getAtlas().getTexture("Panel"));
			this._panelContainer.addChild(this._panel);
			this._panel.pivotX = this._panel.width / 2;
			this._panel.pivotY = this._panel.height / 2;
			this._panel.scaleX = stage.stageWidth * 0.0005;
			this._panel.scaleY = stage.stageWidth * 0.0007
				
			this._playButton = new Button(Assets.getAtlas().getTexture("PlayButton"));
			this._panelContainer.addChild(this._playButton);
			scalingPanel(this._playButton);
			this._playButton.x = - this._panel.width * 0.385;
			this._playButton.y = 0;
			
			this._pauseButton = new Button(Assets.getAtlas().getTexture("PauseButton"));
			this._panelContainer.addChild(this._pauseButton);
			scalingPanel(this._pauseButton);
			this._pauseButton.x = this._playButton.x;
			this._pauseButton.y = 0;
			this._pauseButton.visible = false;
			
			this._previousButton = new Button(Assets.getAtlas().getTexture("BackButton"));
			this._panelContainer.addChild(this._previousButton);
			scalingPanel(this._previousButton);
			this._previousButton.x = this._playButton.x - this._playButton.width * 0.75;
			this._previousButton.y = 0;
			
			this._nextButton = new Button(Assets.getAtlas().getTexture("NextButton"));
			this._panelContainer.addChild(this._nextButton);
			scalingPanel(this._nextButton);
			this._nextButton.x = this._playButton.x + this._playButton.width * 0.75;
			this._nextButton.y = 0;
			
			this._soundButton = new Button(Assets.getAtlas().getTexture("SoundButton"));
			this._panelContainer.addChild(this._soundButton);
			scalingPanel(this._soundButton);
			this._soundButton.x = this._panel.width * 0.45;
			
			this._browseButton = new Button(Assets.getAtlas().getTexture("BrowseButton"));
			this._panelContainer.addChild(this._browseButton);
			scalingPanel(this._browseButton);
			this._browseButton.x = this._panel.width * 0.38;
			this._browseButton.y = this._panel.height * 0.27;
			
			this._addBrowseButton = new Button(Assets.getAtlas().getTexture("BrowseButton"));
			this._panelContainer.addChild(this._addBrowseButton);
			scalingPanel(this._addBrowseButton);
			this._addBrowseButton.x = this._panel.width * 0.28;
			this._addBrowseButton.y = this._panel.height * 0.27;
			
			this._listButton = new Button(Assets.getAtlas().getTexture("ListButton"));
			this._panelContainer.addChild(this._listButton);
			scalingPanel(this._listButton);
			this._listButton.x =  -this._panel.width * 0.24;
			this._listButton.y = this._panel.height * 0.27;
			
			this._panelContainer.pivotX = this._panel.width * 0.45;
			this._panelContainer.pivotY = this._panelContainer.height / 2;
			this._panelContainer.x = stage.stageWidth / 2;
			this._panelContainer.y = stage.stageHeight / 2;
		}
		
		private function scaling(parametr:Object):void
		{
			parametr.pivotX = parametr.width / 2;
			parametr.pivotY = parametr.height / 2;
			parametr.scaleX = parametr.scaleY = stage.stageWidth * 0.00082;
		}
		
		private function scalingPanel(parametr:Object):void
		{
			parametr.pivotX = parametr.width / 2;
			parametr.pivotY = parametr.height / 2;
			parametr.scaleX = parametr.scaleY = stage.stageWidth * 0.0005;
		}
	}
}